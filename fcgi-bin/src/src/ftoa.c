#include "ftoa.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

const static char	table[] = {
	'0',	/* 0x0 */
	'1',	/* 0x1 */
	'2',	/* 0x2 */
	'3',	/* 0x3 */
	'4',	/* 0x4 */
	'5',	/* 0x5 */
	'6',	/* 0x6 */
	'7',	/* 0x7 */
	'8',	/* 0x8 */
	'9',	/* 0x9 */
	'A',	/* 0xA */
	'B',	/* 0xB */
	'C',	/* 0xC */
	'D',	/* 0xD */
	'E',	/* 0xE */
	'F',	/* 0xF */
	'\0'	/* End of string */
};

/*
 *	Data definition:
 */



/*
 *	Function(s) definition:
 */

void ftoASCII(float fl, char* query)
{
     long int p;
     char c[8];
     int j;
     int i;
     
     memcpy(&p, &fl, sizeof(float));
     memset(query, 0, strlen(query));
     
     for(i = 7;i >= 0; i--)
     {
           j = p & 0x0000000f;
           c[i] = table[j];
           p = p >> 4;
     }
     c[8] = 0x0;

     
     memcpy(query,c,sizeof(c));
}

/*---------------------------------------------------------------------*

Name		- 

Usage		- 

Prototype in	- 

Description	- 

*---------------------------------------------------------------------*/
