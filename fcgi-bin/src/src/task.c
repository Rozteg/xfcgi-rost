#include "fcgiost.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <time.h>
#include <errno.h>
#include "net/point.h"
#include "task.h"
#include <pthread.h>


#define	DELAY	2

void receiver(struct _point_ *, struct _pack_ *);

void ToLocalhost (char *requestpointer, int len, int prt);

struct _point_	point[] = {
	{ { 0 }, "", 0, 0, 0, 1,
		{ 		/* struct _sset_ */
			{	/* preset */
				1,	/* seconds */
				0	/* microseconds */
			},
			{	/* count */
				0,	/* seconds */
				0	/* microseconds */
			},
			{ 	/* struct _fdset_ */
				{{ 0 }},	/* read */
				{{ 0 }},	/* write */
				{{ 0 }} 	/* except */
			}, 
			0 	/* error */
		} 
	}
};

struct _pack_	pack[] = {
	{ NULL, 0 }
};

char 		packofjson[2000];


void *task1(void *arg)
{
	
	int	rc;
	//char	str[100];

   	
	point->port = 4583; //was 4581
	point->brdcast = 0;
	

	strcpy (point->name, "127.0.0.1");

		//sprintf(packofjson, "start init\n");
		
		pack->length = sizeof (packofjson);
		pack->entry = malloc (pack->length);
		if (pack->entry != NULL) {
		
			//sprintf(packofjson,"entry != null\n");
			
			rc = setpointsv (point);
			if( rc == POINT_SUCCESS ) {
			
				//sprintf(packofjson,"Setpoint success\n");
				
				while (1) {
				
					rc = selectpointrd(point);
					if ( rc == POINT_SUCCESS ) {
					
					
						sprintf(packofjson,"Selectpoint success\n"); 
						
													
							rc = rdpointfrom(point, pack);
							if ( rc == POINT_SUCCESS ) {
								
								memset (packofjson, 0, sizeof(packofjson));
								memcpy (packofjson, pack->entry, sizeof(packofjson));
								
							}
							
							
					}		
					else {
						//printf ("No Data!\n\n");

					}
					
					sleep(1);
					
				}
				
			}
			else {
				printf ("error\n\n");
			}
		}
		else {
			printf ("No memory to run!\n\n");
		}
		
		closepointsock(point);

	
	return 0;
}

void receiver(struct _point_ *point, struct _pack_ *pack)
{
	int	rc;
		
	fprintf(stderr, "POINT: Server start........\n");
	fprintf(stderr, "POINT: IP-address.......... %s\n", point->name);
	fprintf(stderr, "POINT: Port................ %d\n", point->port);
		
	rc = setpointsv(point);
	if ( rc == POINT_SUCCESS ) {
		do {
			rc = selectpointrd(point);
			if ( rc == POINT_SUCCESS ) {
				rc = rdpointfrom(point, pack);
				if ( rc == POINT_SUCCESS ) {
#if defined (_DeBUG_)
					printb((struct _packb_ *) pack);
#else
					fprintf(stderr, "\n");
					fwrite(pack->entry, 1, point->length, stdout);
					//printf("string: %s\n", (char *) pack->entry);
#endif
					fprintf(stderr, "\nPOINT: received............ %-5d bytes\n", point->length);
				}
			} else {
				fprintf(stderr, ".");
			}
		} while ( rc != POINT_SUCCESS );
		closepointsock(point);
	} else {
		fprintf(stderr, "POINT: Server error........(%x) \'%s\'\n", rc, strerror(errno));
	}
}


void ToLocalhost (char *requestpointer, int len, int prt)
{
	int	rc;
	
	struct _pack_	localpack[] = {
		{ NULL, 0 }
	};
	
	struct _point_	localpoint[] = {
	{ { 0 }, "", 0, 0, 0, 1,
		{ 		/* struct _sset_ */
			{	/* preset */
				1,	/* seconds */
				0	/* microseconds */
			},
			{	/* count */
				0,	/* seconds */
				0	/* microseconds */
			},
			{ 	/* struct _fdset_ */
				{{ 0 }},	/* read */
				{{ 0 }},	/* write */
				{{ 0 }} 	/* except */
			}, 
			0 	/* error */
		} 
	}
};

	strcpy (localpoint->name, "127.0.0.1");
	localpack->length = len;
	localpoint->port = prt;
	//localpoint->port = 5581;
	localpoint->brdcast = 0;


	localpack->entry = requestpointer;

	rc = setpointcl (localpoint);

	if( rc == POINT_SUCCESS )
	{
		rc = wrpointto(localpoint, (struct _pack_ *) localpack);
		if ( rc == POINT_SUCCESS )
		{
			fprintf(stderr, "POINT: sended.............. %-5d bytes\n", localpack->length);
		}
		closepointsock(localpoint);
	}
	else
	{
		printf ("error");
	}
		
	
}