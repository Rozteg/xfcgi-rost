
#include <stdio.h>

#include "opt.h"

/*
 *	Data definition:
 */

static char 	*help[] = {
	"Usage: point [OPTION]\n\n",
	"-V\t--version   version of 'pattern'.\n",
	"-h\t--help      display this help and exit.\n",
	"-v\t--verbose   verbose mode of 'point'.\n",
#if defined ( _USER_OPTION_ )
	"-c\t--client    execute \"CLIENT\" mode. Default is \"SERVER\" mode.\n",	
	"-p\t--port      port number. Default is 4444.\n",	
	"-i\t--ip        IP-address. Default is 127.0.0.1 (localhost)\n",
	"-s\t--size      buffer size. Default is 1024 bytes.\n",	
	"-m\t--message   message for send.\n",
#endif
	NULL,
};

static char	*version[] = {
	"point (Debuging software AOP-08R) ""\n",
	"Written by Andrey Dronov.\n\n",
	NULL,
};

static int	verbose = 0;
#if defined ( _USER_OPTION_ )
static int	flag = 0;
static int	port = 4444;
static int	size = 1024;
static char	ip[32] = "127.0.0.1";
static char	message[256] = "";
#endif

struct _option_ option[] = {
	{ 1, "-V", "--version",  version, 	GETOPT_FLAG, 	(setopt_t) getmsg },
	{ 2, "-h", "--help",     help,    	GETOPT_FLAG, 	(setopt_t) getmsg },
	{ 3, "-v", "--verbose", &verbose, 	GETOPT_FLAG, 	(setopt_t) setinc },
#if defined ( _USER_OPTION_ )
	{ 4, "-c", "--client",  &flag, 		GETOPT_FLAG, 	(setopt_t) setflag },
	{ 5, "-p", "--port", 	&port, 		GETOPT_NUM, 	(setopt_t) setnum },
	{ 6, "-i", "--ip", 	ip, 		GETOPT_MSG, 	(setopt_t) setstr },
	{ 7, "-m", "--message", message, 	GETOPT_MSG, 	(setopt_t) setstr },
	{ 8, "-s", "--size", 	&size, 		GETOPT_NUM, 	(setopt_t) setnum },
#endif
	{ 0, NULL, NULL, NULL, 0, NULL }
};

/*
 *	Function(s) definition:
 */

/*---------------------------------------------------------------------*

Name		-

Usage		- 

Prototype in	-     

Description	-     

*---------------------------------------------------------------------*/
void puthelp(void)
{
	puthlp(help);
}

