#ifndef _PACK_H
#define _PACK_H



/*
 *	Macros difinition
 */

#define _GETLEN(x)	( sizeof ( x ) / sizeof ( x[0] ) )


/*
 *	Type declaration
 */

 
struct _pack_ {
	void 	*entry;
	int	length;
};

/*
 *	Data declaration
 */



/*
 *	Function declaration
 */

#ifdef __cplusplus
extern "C" {
#endif


#ifdef __cplusplus
}
#endif

#endif // _PACK_H
