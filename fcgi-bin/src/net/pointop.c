
#include <sys/socket.h>
#include <string.h>

#include <net/point.h>

#if defined (_DEBUG_)
#include <stdio.h>
#endif

/*
 *	Data definition:
 */


/*
 *	Function(s) definition:
 */

/*---------------------------------------------------------------------*

Name		-

Usage		- 

Prototype in	-     

Description	- 

*---------------------------------------------------------------------*/
int wrpointto(struct _point_ *point, struct _pack_ *pack)
{
	int	rc;

	rc = sendto(point->socket, pack->entry, pack->length, 0, (struct sockaddr *) &point->host, sizeof ( point->host ));

	if ( rc >= 0 ) {
#if defined (_DEBUG_)
		fprintf(stderr, "POINT:   to \'%s:%d\' sended..... %5d bytes \n", point->name, point->port, rc);
#endif
	
		point->length = rc;
		rc = POINT_SUCCESS;
	} else {
#if defined (_DEBUG_)
		perror("sendto");
#endif
		point->length = 0;
		rc = POINT_SEND_ERR;
	}

	return rc;
}

/*---------------------------------------------------------------------*

Name		-

Usage		- 

Prototype in	-     

Description	- ��������� ������������ �������� ������ select()

*---------------------------------------------------------------------*/
void setpointu(struct _point_ *point, long t)
{
	setssetu(&point->sset, t);
}

/*---------------------------------------------------------------------*

Name		-

Usage		- 

Prototype in	-     

Description	- 

*---------------------------------------------------------------------*/
int selectpointrd(struct _point_ *point)
{
	return getsset(&point->sset, point->socket);
}

/*---------------------------------------------------------------------*

Name		-

Usage		- 

Prototype in	-     

Description	- 

*---------------------------------------------------------------------*/
int rdpointfrom(struct _point_ *point, struct _pack_ *pack)
{
	int		rc;
	unsigned int	length = sizeof ( point->host );

	rc = recvfrom(point->socket, pack->entry, pack->length, 0, (struct sockaddr *) &point->host, &length);
	if ( rc >= 0 ) {
#if defined (_DEBUG_)
		char	name[32];

		strncpy(name, inet_ntoa(point->host.sin_addr), 32);
		fprintf(stderr, "POINT: from \'%s:%d\' received... %5d bytes\n", name, 
				htons(point->host.sin_port), rc);
#endif
		point->length = rc;
		rc = POINT_SUCCESS;
	} else {
#if defined (_DEBUG_)
		perror("recvfrom");
#endif
		point->length = 0;
		rc = POINT_RECV_ERR;
	}

	return rc;
}

