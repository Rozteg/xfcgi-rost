#ifndef _NET_POINT_H
#define _NET_POINT_H

#include <net/pointss.h>
#include <net/pointerr.h>

#include <pack.h>

/*
 *	Macros difinition
 */


/*
 *	Type declaration
 */

struct _point_ {
	struct sockaddr_in	host;
	char			name[32];	/* IP-address*/
	int			port;		/* Port*/
	int			socket;		/* Socket ID*/
	int			length;		/**/
	int			brdcast;	/* ���� ��������� ������������������ ������ 1 - TRUE 0 - FALSE*/
	struct _sset_		sset;		/**/
};

/*
 *	Data declaration
 */



/*
 *	Function declaration
 */

#ifdef __cplusplus
extern "C" {
#endif

int setpointcl(struct _point_ *);
	
int setpointsv(struct _point_ *);
	
int selectpointrd(struct _point_ *);

void setpointu(struct _point_ *, long);

int closepointsock(struct _point_ *);

int wrpointto(struct _point_ *, struct _pack_ *);

int rdpointfrom(struct _point_ *, struct _pack_ *);

#ifdef __cplusplus
}
#endif

#endif /* _NET_POINT_H */
