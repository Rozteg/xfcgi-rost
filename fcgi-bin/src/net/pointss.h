#ifndef _POINT_SELECT_SET_H
#define _POINT_SELECT_SET_H

#include <arpa/inet.h>
#include <time.h>

/*
 *	Macros difinition
 */



/*
 *	Type declaration
 */

struct _fdset_ {
	fd_set	read;
	fd_set	write;
	fd_set	except;
};

/*
	SELECT SET - ��������� ���������� ������ select()
*/
struct _sset_ {
	struct timeval	preset;
	struct timeval	count;
	struct _fdset_	fd;
	int		error;
};

/*
 *	Data declaration
 */



/*
 *	Function declaration
 */

#ifdef __cplusplus
extern "C" {
#endif

void setssetu(struct _sset_ *, long);

void setssets(struct _sset_ *, long);

int getsset(struct _sset_ *, int);

#ifdef __cplusplus
}
#endif

#endif /* _POINT_SELECT_SET_H */
