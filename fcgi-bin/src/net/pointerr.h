#ifndef _POINT_ERROR_H
#define _POINT_ERROR_H



/*
 *	Macros difinition
 */



/*
 *	Type declaration
 */

enum {
	POINT_SUCCESS = 0,
	POINT_HOST_ERR,
	POINT_PORT_ERR,
	POINT_INIT_ERR,
	POINT_SOCK_ERR,
	POINT_BIND_ERR,
	POINT_SEND_ERR,
	POINT_RECV_ERR,
	POINT_TOUT_ERR,
	POINT_SOTH_ERR,
	POINT_SLCT_ERR,
	POINT_SOPT_ERR,
	POINT_CLOSE_ERR
};

/*
 *	Data declaration
 */



/*
 *	Function declaration
 */

#ifdef __cplusplus
extern "C" {
#endif


#ifdef __cplusplus
}
#endif

#endif /* _POINT_ERROR_H */
