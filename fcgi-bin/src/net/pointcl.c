
#include <string.h>

#include <net/pointco.h>

#if defined (_DEBUG_)
#include <stdio.h>
#endif

/*
 *	Data definition:
 */



/*
 *	Function(s) definition:
 */

/*---------------------------------------------------------------------*

Name		-

Usage		- 

Prototype in	-     

Description	- 

*---------------------------------------------------------------------*/
int setpointcl(struct _point_ *p)
{
	int	rc;
	
	bzero(&p->host, sizeof ( p->host ));
	
	rc = setpointaddr(p);
	if ( rc == POINT_SUCCESS ) {
		rc = setpointport(p);
		if ( rc == POINT_SUCCESS ) {
			rc = setpointsock(p);
			if ( rc == POINT_SUCCESS ) {
				rc = setpointsopt(p);
			}
		}
	}
	
	return rc;
}

