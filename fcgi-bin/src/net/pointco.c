
#include <netdb.h>
#include <sys/socket.h>
#include <unistd.h>

#include <net/point.h>


#if defined (_DEBUG_)
#include <stdio.h>
#endif

/*
 *	Data definition:
 */



/*
 *	Function(s) definition:
 */

/*---------------------------------------------------------------------*

Name		-

Usage		- 

Prototype in	-     

Description	-     

*---------------------------------------------------------------------*/
int setpointaddr(struct _point_ *p)
{
	struct hostent	*host;
	int 		rc;
	
#if defined (_DEBUG_)
	fprintf(stderr, "Name is ....... \'%s\'\n", p->name);
#endif
	if ( p->name[0] != '\0' ) {
		/* ������� ��� ����� � ���������� ������� � ����������� ��� */
		rc = inet_aton(p->name, &p->host.sin_addr);
		if ( rc != 0 ) {
			rc = POINT_SUCCESS;
		} else {
#if defined (_DEBUG_)
			perror("inet_aton");
#endif
			/* ���� ������ �������� ����� �� ����� /etc/hosts */
			host = gethostbyname(p->name);
			if ( host != NULL ) {
				p->host.sin_addr = *((struct in_addr *) host->h_addr);
				rc = POINT_SUCCESS;
			} else {
#if defined (_DEBUG_)
				perror("gethostbyname");
#endif
				rc = POINT_HOST_ERR;
			}
		}
	} else {
		p->host.sin_addr.s_addr = INADDR_ANY;
		rc = POINT_SUCCESS;
	}
	
	return rc;
}

/*---------------------------------------------------------------------*

Name		-

Usage		- 

Prototype in	-     

Description	- 

*---------------------------------------------------------------------*/
int setpointport(struct _point_ *p)
{
	int	rc;
	
	p->host.sin_family = AF_INET;
	if ( p->port != 0 ) {
		p->host.sin_port = htons(p->port);
		rc = POINT_SUCCESS;
	} else {
#if defined (_DEBUG_)
		fprintf(stderr, "port value zero\n");
#endif
		rc = POINT_PORT_ERR;
	}
	
	return rc;
}

/*---------------------------------------------------------------------*

Name		-

Usage		- 

Prototype in	-     

Description	- 

*---------------------------------------------------------------------*/
int setpointsock(struct _point_ *p)
{
	int	rc;
	
	p->socket = socket(AF_INET, SOCK_DGRAM, 0);
	if ( p->socket != (-1) ) {
		rc = POINT_SUCCESS;
#if defined (_DEBUG_)
		fprintf(stderr, "POINT: socket=%d\n", p->socket);
#endif
	} else {
#if defined (_DEBUG_)
		perror("socket");
#endif
		rc = POINT_SOCK_ERR;
	}
	
	return rc;
}

/*---------------------------------------------------------------------*

Name		-

Usage		- 

Prototype in	-     

Description	- 

*---------------------------------------------------------------------*/
int setpointsopt(struct _point_ *p)
{
	const int	value = 1;
	int		rc;
	
#if defined (_DEBUG_)
	fprintf(stderr, "Set broadcat mode %d\n", p->brdcast);
#endif
	if ( p->brdcast == 1 ) {
		/* ���������� �������� 1 ��� ��������� ������ SO_BROADCAST */
		rc = setsockopt(p->socket, SOL_SOCKET, SO_BROADCAST, &value, sizeof ( value ));
		if ( rc == 0 ) {
			rc = POINT_SUCCESS;
		} else {
#if defined (_DEBUG_)
			perror("setsockopt");
#endif	
			rc = POINT_SOPT_ERR;
		}
	} else {
		rc = POINT_SUCCESS;
	}
	
	return rc;
}

/*---------------------------------------------------------------------*

Name		-

Usage		- 

Prototype in	-     

Description	- 

*---------------------------------------------------------------------*/
int closepointsock(struct _point_ *p)
{
	int	rc;
	
	rc = close(p->socket);
	if ( rc == 0 ) {
		rc = POINT_SUCCESS;
	} else {
#if defined (_DEBUG_)
		perror("close");
#endif
		rc = POINT_CLOSE_ERR;
	}
	
	return rc;
}

