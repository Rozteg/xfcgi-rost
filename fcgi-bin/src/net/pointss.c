
#include <string.h>

#if defined (_DEBUG_)
#include <stdio.h>
#endif

#include <net/pointss.h>
#include <net/pointerr.h>

/*
 *	Data definition:
 */



/*
 *	Function(s) definition:
 */

/*---------------------------------------------------------------------*

Name		-

Usage		- 

Prototype in	-     

Description	- 

*---------------------------------------------------------------------*/
void setssetu(struct _sset_ *s, long t)
{
	s->preset.tv_usec = t;
}


/*---------------------------------------------------------------------*

Name		-

Usage		- 

Prototype in	-     

Description	- 

*---------------------------------------------------------------------*/
void setssets(struct _sset_ *s, long t)
{
	s->preset.tv_sec = t;
}

/*---------------------------------------------------------------------*

Name		-

Usage		- 

Prototype in	-     

Description	- 

*---------------------------------------------------------------------*/
int getsset(struct _sset_ *s, int n)
{
	int	rc;

	FD_ZERO(&s->fd.read);
	
	FD_SET(n, &s->fd.read);

#if defined (_DEBUG_)
	fprintf(stderr, "POINT: preset=%lu count=%lu\n", s->preset.tv_usec, s->count.tv_usec);
#endif
	memcpy(&s->count, &s->preset, sizeof ( struct timeval ) );
#if defined (_DEBUG_)
	fprintf(stderr, "POINT: preset=%lu count=%lu\n", s->preset.tv_usec, s->count.tv_usec);
#endif
	rc = select(n + 1, &s->fd.read, NULL, NULL, &s->count);
#if defined (_DEBUG_)
	fprintf(stderr, "POINT: select = %d\n", rc);
#endif
	if ( rc > 0 ) {
		if ( FD_ISSET(n, &s->fd.read) ) {
			rc = POINT_SUCCESS;	
		} else {
			rc = POINT_SOTH_ERR;
		}
	} else if ( rc == 0 ) {
		rc = POINT_TOUT_ERR;
	} else {
		rc = POINT_SLCT_ERR;
	}
	
	
	return rc;
}

