
#include <sys/socket.h>
#include <string.h>

#include <net/pointco.h>

#if defined (_DEBUG_)
#include <stdio.h>
#include <fcgi/fcgi_stdio.h>
#endif

/*
 *	Data definition:
 */

//char errstr[1024];

/*
 *	Function(s) definition:
 */

/*---------------------------------------------------------------------*

Name		-

Usage		- 

Prototype in	-     

Description	- ��������� ����� � ���� � �������

*---------------------------------------------------------------------*/
static int setpointbind(struct _point_ *p)
{
	int	rc;
	
	rc = bind(p->socket, (struct sockaddr *) &p->host, sizeof ( p->host ));
	if ( rc == 0 ) {
		rc = POINT_SUCCESS;
	} else {
#if defined (_DEBUG_)
		perror("bind");
#endif
		rc = POINT_BIND_ERR;

	}

	return rc;
}

/*---------------------------------------------------------------------*

Name		-

Usage		- 

Prototype in	-     

Description	- 

*---------------------------------------------------------------------*/
int setpointsv(struct _point_ *p)
{
	int	rc;
	
	bzero(&p->host, sizeof ( p->host ));
	
	rc = setpointaddr(p);
	if ( rc == POINT_SUCCESS ) {
		rc = setpointport(p);
		if ( rc == POINT_SUCCESS ) {
			rc = setpointsock(p);
			if ( rc == POINT_SUCCESS ) {
				rc  = setpointbind(p);
			}
		}
	}

	return rc;
}

