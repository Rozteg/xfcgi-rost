
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>

#include <recover/asctol.h>

#include <net/point.h>

/*
 *	Data definition:
 */


/*
 *	Function(s) definition:
 */

/*---------------------------------------------------------------------*

Name		-

Usage		- 

Prototype in	-     

Description	-     

*---------------------------------------------------------------------*/
static int setpnt(struct _point_ *point)
{
	struct hostent	*host;
	int 		rc;
	
	// ������� ��� ����� � ���������� ������� � ����������� ��� 
	rc = inet_aton(point->name, &point->host.sin_addr);
	if ( rc == 0 ) {
		// ���� ������ �������� ����� �� ����� /etc/hosts
		host = gethostbyname(point->name);
		if ( host == NULL ) {
			rc = POINT_HOST_ERR;
		} else {
			point->host.sin_addr = *((struct in_addr *) host->h_addr);
			rc = POINT_SUCCESS;
		}
	} else {
		rc = POINT_SUCCESS;
	}
	
	return rc;
}

/*---------------------------------------------------------------------*

Name		-

Usage		- 

Prototype in	-     

Description	- ��������� ����� �������    

*---------------------------------------------------------------------*/
int setspnt(int *s, struct _point_ *point)
{
	int	rc;
	
	bzero(&point->host, sizeof ( point->host ));
	
	rc = setpnt(point);
	if ( rc == POINT_SUCCESS ) {
		point->host.sin_family = AF_INET;
		if ( point->port != 0 ) {
			point->host.sin_port = htons(point->port);
			*s = socket(AF_INET, SOCK_DGRAM, 0);
			if ( *s == (-1) ) {
				rc = POINT_INIT_ERR;
			}
		} else {
			rc = POINT_PORT_ERR;
		}
	}
	
	return rc;
}

/*---------------------------------------------------------------------*

Name		-

Usage		- 

Prototype in	-     

Description	- ��������� ����� �������    

*---------------------------------------------------------------------*/
int setrpnt(int *s, struct _point_ *point)
{
	int	rc;
	
	bzero(&point->host, sizeof ( point->host ));
	
	point->host.sin_addr.s_addr = INADDR_ANY;
	point->host.sin_family = AF_INET;
	if ( point->port != 0 ) {
		point->host.sin_port = htons(point->port);
		*s = socket(AF_INET, SOCK_DGRAM, 0);
		if ( *s == (-1) ) {
			rc = POINT_INIT_ERR;
		} else {
			//	��������� ����� � ���� � �������
			rc = bind(*s, (struct sockaddr *) &point->host, sizeof ( point->host ));
			if ( rc != 0 ) {
				rc = POINT_BIND_ERR;
			} else {
				rc = POINT_SUCCESS;
			}
		}
	}

	return rc;
}

/*---------------------------------------------------------------------*

Name		-

Usage		- 

Prototype in	-     

Description	- �������� ������ ����� �����    

*---------------------------------------------------------------------*/
int writepnt(int s, void *b, int l, struct _point_ *point)
{
	int	rc;

	rc = sendto(s, b, l, 0, (struct sockaddr *) &point->host, sizeof ( point->host ));
	
	if ( rc < 0 ) {
#if defined (_DEBUG_)
		perror("POINT");
#endif
		rc = POINT_SEND_ERR;
	} else {
#if defined (_DEBUG_)
		printf("Sended......... %-5d bytes\n", rc);
#endif
		rc = POINT_SUCCESS;
	}

	close(s);

	return rc;
}

/*---------------------------------------------------------------------*

Name		-

Usage		- 

Prototype in	-     

Description	- ������� ������ ����� �����    

*---------------------------------------------------------------------*/
int readpnt(int s, void *b, int l, struct _point_ *point)
{
	int		rc;
	unsigned int	length = sizeof ( point->host );

	rc = recvfrom(s, b, l, 0, (struct sockaddr *) &point->host, &length);
	if ( rc < 0 ) {
		rc = POINT_RECV_ERR;
#if defined (_DEBUG_)
		perror("POINT");
#endif
	} else {
#if defined (_DEBUG_)
		printf("Received....... %-5d bytes\n", rc);
#endif
		rc = POINT_SUCCESS;
	}

	close(s);

	return rc;
}

