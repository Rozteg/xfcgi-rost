#ifndef _POINT_COMMON_H
#define _POINT_COMMON_H

#include <net/point.h>

/*
 *	Macros difinition
 */



/*
 *	Type declaration
 */



/*
 *	Data declaration
 */
//extern char errstr[1024];


/*
 *	Function declaration
 */

#ifdef __cplusplus
extern "C" {
#endif

int setpointaddr(struct _point_ *);
	
int setpointport(struct _point_ *);

int setpointsock(struct _point_ *);

int setpointsopt(struct _point_ *);

#ifdef __cplusplus
}
#endif

#endif /* _POINT_COMMON_H */
