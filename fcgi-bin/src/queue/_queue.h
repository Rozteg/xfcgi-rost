#ifndef __QUEUE_H
#define __QUEUE_H

/*
 *	���������� ������ QUEUE
 */

/*
 *	Macros difinition
 */



/*
 *	Type declaration
 */

struct _queue_ {
	char	*head;
	char	*tail;
	char	*ptr;
};

enum {   
	EOQUEUE = (-1),
	SUCCESS = 0
};

enum {
	QUEUE_DUMMY = (-1),
	QUEUE_HEAD = 0,
	QUEUE_TAIL
};

/*
 *	Data declaration
 */



/*
 *	Function declaration
 */

#ifdef __cplusplus
extern "C" {
#endif

void seekq(struct _queue_ *, int);

#ifdef __cplusplus
}
#endif

#endif // __QUEUE_H
