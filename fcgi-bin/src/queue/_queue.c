
#include <queue/_queue.h>


/*
 *	Data definition:
 */


 	
/*
 *	Function(s) definition:
 */

/*---------------------------------------------------------------------*

Name		-

Usage		- 

Prototype in	-     

Description	-     

*---------------------------------------------------------------------*/
void seekq(struct _queue_ *q, int n)
{
	switch ( n ) {
		case QUEUE_HEAD :
			q->ptr = q->head;
			break;
		case QUEUE_TAIL :
			q->ptr = q->tail;
			break;
		default :
			break;
	} 
}
