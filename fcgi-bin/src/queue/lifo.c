
#include <queue/lifo.h>


/*
 *	Data definition:
 */


 	
/*
 *	Function(s) definition:
 */

/*---------------------------------------------------------------------*

Name		-

Usage		- 

Prototype in	-     

Description	-     

*---------------------------------------------------------------------*/
void setlifo(LIFO *l, char *s, char *f)
{
	// ������������� ��������� ������ ������� � �������� �������� 
	// ������� �� ������� �������������� ������ ������
	
	l->head = l->ptr = --s;
	
	// ������������� ��������� ������ ������� �� ��������� ������� 
	// ������

	l->tail = f;
}

/*---------------------------------------------------------------------*

Name		-

Usage		- 

Prototype in	-     

Description	-     

*---------------------------------------------------------------------*/
int putlifo(LIFO *q, int s)
{                  
	int	rc;

	if ( q->ptr <= q->tail ) {
		q->ptr++;
		*q->ptr = (char) s;
		rc = SUCCESS;
	} else {
		rc = EOQUEUE;	
	}

	return rc;
}

/*---------------------------------------------------------------------*

Name		-

Usage		- 

Prototype in	-     

Description	-     

*---------------------------------------------------------------------*/
int getlifo(LIFO *q)
{
	int	rc;

	if ( q->ptr > q->head ) {
		rc = (unsigned) *q->ptr;
		q->ptr--;
	} else {
		rc = EOQUEUE;
	}

	return rc;
}
