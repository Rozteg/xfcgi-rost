#ifndef _LIFO_H
#define _LIFO_H

#include <queue/_queue.h>

/*
 *	Macros difinition
 */



/*
 *	Type declaration
 */

typedef struct _queue_	LIFO;

/*
 *	Data declaration
 */



/*
 *	Function declaration
 */

#ifdef __cplusplus
extern "C" {
#endif

void setlifo(LIFO *, char *, char *);

int putlifo(LIFO *, int);

int getlifo(LIFO *);

#ifdef __cplusplus
}
#endif

#endif // _LIFO_H
