
#include <queue/fifo.h>


/*
 *	Data definition:
 */


 	
/*
 *	Function(s) definition:
 */

/*---------------------------------------------------------------------*

Name		-

Usage		- 

Prototype in	-     

Description	-     

*---------------------------------------------------------------------*/
int putfifo(FIFO *f, int s)
{
	int	rc;

	if ( f->ptr <= f->tail ) {
		*f->ptr = (char) s;
		f->ptr++;
		rc = SUCCESS;
	} else {
		rc = EOQUEUE;	
	}

	return rc;
}

/*---------------------------------------------------------------------*

Name		-

Usage		- 

Prototype in	-     

Description	-     

*---------------------------------------------------------------------*/
int getfifo(FIFO *f)
{
	int	rc;

	if ( f->ptr <= f->tail ) {
		rc = (unsigned) *f->ptr;
		f->ptr++;
	} else {
		rc = EOQUEUE;
	}

	return rc;
}

