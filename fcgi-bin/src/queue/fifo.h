#ifndef _FIFO_H
#define _FIFO_H

#include <queue/_queue.h>

/*
 *	Macros difinition
 */



/*
 *	Type declaration
 */

typedef struct _queue_	FIFO;

/*
 *	Data declaration
 */



/*
 *	Function declaration
 */

#ifdef __cplusplus
extern "C" {
#endif

int putfifo(FIFO *, int);

int getfifo(FIFO *);

#ifdef __cplusplus
}
#endif

#endif // _FIFO_H
