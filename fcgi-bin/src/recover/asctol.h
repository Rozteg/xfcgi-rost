#ifndef _ASCII_TO_LONG_H
#define _ASCII_TO_LONG_H



/*
 *	Macros difinition
 */



/*
 *	Type declaration
 */



/*
 *	Data declaration
 */



/*
 *	Function declaration
 */

#ifdef __cplusplus
extern "C" {
#endif

long asctol(char *);

long ascbtol(char *);

#ifdef __cplusplus
}
#endif

#endif // _ASCII_TO_LONG_H
