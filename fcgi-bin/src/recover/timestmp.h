#ifndef _TIMESTAMP_H
#define _TIMESTAMP_H

#include <time.h>

/*
 *	Macros difinition
 */



/*
 *	Type declaration
 */

union _tm_ {
	struct tm	tm;
	int		v[sizeof ( struct tm )];
};

/*
 *	Data declaration
 */



/*
 *	Function declaration
 */

#ifdef __cplusplus
extern "C" {
#endif

int strtotm(union _tm_ *, const char *);

#ifdef __cplusplus
}
#endif

#endif // _TIMESTAMP_H
