#ifndef __ASCII_TO_LONG_H
#define __ASCII_TO_LONG_H



/*
 *	Macros difinition
 */



/*
 *	Type declaration
 */

struct _number_ {
	int	sign;
	int	base;
	int	exp;
	long	result;
};

/*
 *	Data declaration
 */



/*
 *	Function declaration
 */

#ifdef __cplusplus
extern "C" {
#endif


#ifdef __cplusplus
}
#endif

#endif // __ASCII_TO_LONG_H
