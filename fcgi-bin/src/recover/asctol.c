
#include <recover/_asctol.h>
#include <queue/lifo.h>

/*
 *	Data definition:
 */



/*
 *	Function(s) definition:
 */

/*---------------------------------------------------------------------*

Name		-

Usage		- 

Prototype in	-     

Description	-     

*---------------------------------------------------------------------*/
static long pow(long b, int exp)
{
	long	base = b;
	
	if ( exp > 0 ) {
		while ( --exp ) {
			b *= base;
		}
	} else if ( exp == 0 ) {
		b = 1;
	} else {
		b = (-1);
	}
	
	return b;
}

/*---------------------------------------------------------------------*

Name		-

Usage		- 

Prototype in	-     

Description	-     

*---------------------------------------------------------------------*/
static int getdig(int s)
{
	int	rc;

	switch ( s ) {
		case '0' :
			rc = 0;
			break;
		case '1' :
			rc = 1;
			break;
		case '2' :
			rc = 2;
			break;
		case '3' :
			rc = 3;
			break;
		case '4' :
			rc = 4;
			break;
		case '5' :
			rc = 5;
			break;
		case '6' :
			rc = 6;
			break;
		case '7' :
			rc = 7;
			break;
		case '8' :
			rc = 8;
			break;
		case '9' :
			rc = 9;
			break;
		case 'A' :
		case 'a' :
			rc = 10;
			break;
		case 'B' :
		case 'b' :
			rc = 11;
			break;
		case 'C' :
		case 'c' :
			rc = 12;
			break;
		case 'D' :
		case 'd' :
			rc = 13;
			break;
		case 'E' :
		case 'e' :
			rc = 14;
			break;
		case 'F' :
		case 'f' :
			rc = 15;
			break;
	}

	return rc;
}

/*---------------------------------------------------------------------*

Name		-

Usage		- 

Prototype in	-     

Description	-     

*---------------------------------------------------------------------*/
static void setihex(LIFO *l, char *s, struct _number_ *n)
{
	while ( *s != '\0' ) {
		switch ( *s ) {
			case '-' :
				n->sign = 1;
				break;
			case 'H' :
			case 'h' :
			case 'X' :
			case 'x' :
				n->base = 16;
				break;
			case '0' :
			case '1' :
			case '2' :
			case '3' :
			case '4' :
			case '5' :
			case '6' :
			case '7' :
			case '8' :
			case '9' :
			case 'A' :
			case 'B' :
			case 'C' :
			case 'D' :
			case 'E' :
			case 'F' :
			case 'a' :
			case 'b' :
			case 'c' :
			case 'd' :
			case 'e' :
			case 'f' :
				putlifo(l, *s);
				break;
		}
		s++;
	}
}

/*---------------------------------------------------------------------*

Name		-

Usage		- 

Prototype in	-     

Description	-     

*---------------------------------------------------------------------*/
static void setbin(LIFO *l, char *s, struct _number_ *n)
{
	while ( *s != '\0' ) {
		switch ( *s ) {
			case 'B' :
			case 'b' :
				n->base = 2;
				break;
			case '0' :
			case '1' :
				putlifo(l, *s);
				break;
		}
		s++;
	}
}

/*---------------------------------------------------------------------*

Name		-

Usage		- 

Prototype in	-     

Description	-     

*---------------------------------------------------------------------*/
static long ascto(char *s, void (*recover)(LIFO *, char *, struct _number_ *))
{
	char		buffer[16];
	LIFO		lifo;
	int		i;
	struct _number_	number = { 0, 10, 0, 0 };
	
	setlifo(&lifo, buffer, &buffer[15]);

	recover(&lifo, s, &number);
	
	while ( (i = getlifo(&lifo)) != EOQUEUE ) {
		number.result += (long) getdig(i) * pow(number.base, number.exp);
		number.exp++;
	}

	return ( number.result * pow((-1), number.sign) ); 
}

/*---------------------------------------------------------------------*

Name		-

Usage		- 

Prototype in	-     

Description	-     

*---------------------------------------------------------------------*/
long asctol(char *s)
{
	return ascto(s, setihex);
}

/*---------------------------------------------------------------------*

Name		-

Usage		- 

Prototype in	-     

Description	-     

*---------------------------------------------------------------------*/
long ascbtol(char *s)
{
	return ascto(s, setbin);
}

