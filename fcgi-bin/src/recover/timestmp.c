
#include <stdlib.h>
#include <string.h>

#include <recover/asctol.h>
#include <recover/timestmp.h>
#include <recover/tstmpmap.h>

#if defined (_DEBUG_)
#include <stdio.h>
#endif

/*
 *	Function(s) definition:
 */

/*---------------------------------------------------------------------*

Name		-

Usage		- 

Prototype in	-     

Description	-	

*---------------------------------------------------------------------*/
#if defined (_DEBUG_)
static void print(const char *s, int v)
{
	fprintf(stderr, "%16s = %u\n", s, v);
}
#endif

/*---------------------------------------------------------------------*

Name		-

Usage		- 

Prototype in	-     

Description	-	

*---------------------------------------------------------------------*/
static int sub(struct _tsmap_ *m, int v)
{
#if defined (_DEBUG_)
	print(m->name, v - m->bias);
#endif
	return (v - m->bias);
}

/*
 *	Data definition:
 */

static struct _tsmap_	map[] = {
	{ 5, 1900,	'-',  sub, "tm_year" }, 
	{ 4, 1,   	'-',  sub, "tm_month" }, 
	{ 3, 0,   	' ',  sub, "tm_mday" }, 
	{ 2, 0,   	':',  sub, "tm_hour" }, 
	{ 1, 0,   	':',  sub, "tm_min" }, 
	{ 0, 0,   	'\0', sub, "tm_sec" }, 
};

/*
 *	Function(s) definition:
 */

/*---------------------------------------------------------------------*

Name		-

Usage		- 

Prototype in	-     

Description	-	���������� struct tm ���������� �� ������ ������� 
			"YYYY-MM-DD hh:mm:ss"    

*---------------------------------------------------------------------*/
int strtotm(union _tm_ *tm, const char *ts)
{
	char		*s, *str, *start;
	struct  _tsmap_	*m  = map;
	int		i, rc;
	
	s = str = start = malloc(strlen(ts) + 1);

	if ( s != NULL ) {
		
		strcpy(s, ts);

		for ( i = 0; i < ( sizeof ( map ) / sizeof ( map[0] ) ); i++ ) {
			s = strchr(s, m->sep);
			*s = '\0';
			tm->v[m->i] = m->set(m, asctol(str));
			s++;
			str = s;
			m++;
		}
		free(start);
		rc = 0;		
	} else {
		rc = (-1);
	}

	return rc;
}

