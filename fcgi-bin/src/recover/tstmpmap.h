#ifndef _TIME_STAMP_MAP_H
#define _TIME_STAMP_MAP_H



/*
 *	Macros difinition
 */



/*
 *	Type declaration
 */

struct _tsmap_ {
	int	i;
	int	bias;
	int	sep;
	int 	(*set)(struct _tsmap_ *, int);
	char	*name;
};

/*
 *	Data declaration
 */



/*
 *	Function declaration
 */

#ifdef __cplusplus
extern "C" {
#endif


#ifdef __cplusplus
}
#endif

#endif // _TIME_STAMP_MAP_H
