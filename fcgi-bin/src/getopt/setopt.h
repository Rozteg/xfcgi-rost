#ifndef _SET_OPTION_H
#define _SET_OPTION_H

#include <getopt/option.h>

/*
 *	Macros difinition
 */



/*
 *	Type declaration
 */

typedef int (*setopt_t)(struct _option_ *, void *);

/*
 *	Data declaration
 */



/*
 *	Function declaration
 */

#ifdef __cplusplus
extern "C" {
#endif

int puthlp(char **);

int getmsg(struct _option_ *, char *);

int setstr(struct _option_ *, char *);

int setnum(struct _option_ *, char *);

int setflag(struct _option_ *, char *);

int setinc(struct _option_ *, char *);

#ifdef __cplusplus
}
#endif

#endif // _SET_OPTION_H
