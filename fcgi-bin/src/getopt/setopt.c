
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <getopt/option.h>
#include <recover/asctol.h>

/*
 *	Data definition:
 */



/*
 *	Function(s) definition:
 */

/*---------------------------------------------------------------------*

Name		-

Usage		- 

Prototype in	-     

Description	-     

*---------------------------------------------------------------------*/
void puthlp(char **msg)
{
	while ( *msg ) {
		fprintf(stderr, "%s", *msg);
		msg++;
	}
	
	exit(0);
}

/*---------------------------------------------------------------------*

Name		-

Usage		- 

Prototype in	-     

Description	-     

*---------------------------------------------------------------------*/
int setstr(struct _option_ *opt, char *s)
{
	int	rc = 0;
	
	strcpy(opt->value, s);

	return rc;
}

/*---------------------------------------------------------------------*

Name		-

Usage		- 

Prototype in	-     

Description	-     

*---------------------------------------------------------------------*/
int setnum(struct _option_ *opt, char *s)
{
	int	rc = 0;

	*((long *) opt->value) = asctol(s);

	return rc;
}

#if defined ( _DOS_ )
#pragma warn -par
#endif
/*---------------------------------------------------------------------*

Name		-

Usage		- 

Prototype in	-     

Description	-     

*---------------------------------------------------------------------*/
int getmsg(struct _option_ *opt, char *s)
{
	puthlp(opt->value);
	
	return 0;
}

/*---------------------------------------------------------------------*

Name		-

Usage		- 

Prototype in	-     

Description	-     

*---------------------------------------------------------------------*/
int setflag(struct _option_ *opt, char *s)
{
	int	rc = 0;

	*((long *) opt->value) = 1;

	return rc;
}

/*---------------------------------------------------------------------*

Name		-

Usage		- 

Prototype in	-     

Description	-     

*---------------------------------------------------------------------*/
int setinc(struct _option_ *opt, char *s)
{
	int	rc = 0;

	*((long *) opt->value) += 1;

	return rc;
}
#if defined ( _DOS_ )
#pragma warn +par
#endif
