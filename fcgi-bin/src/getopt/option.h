#ifndef _OPTION_H
#define _OPTION_H



/*
 *	Macros difinition
 */


/*
 *	Type declaration
 */

struct _option_ {
	int	id;
	char	*sname;	// short name
	char	*lname;	// long name
	void 	*value;
	int	type;
	int	(*handler)(struct _option_ *, void *);
};

/*
 *	Data declaration
 */



/*
 *	Function declaration
 */

#ifdef __cplusplus
extern "C" {
#endif


#ifdef __cplusplus
}
#endif

#endif // _OPTION_H
