#ifndef _OPT_H
#define _OPT_H

#include <getopt/getopt.h>

/*
 *	Macros difinition
 */


/*
 *	Type declaration
 */

enum {
	OPT_VERSION = 0,
	OPT_HELP = 1,
	OPT_VERBOSE = 2,
	OPT_CLIENT,
	OPT_PORT,
	OPT_IP,
	OPT_MSG,
	OPT_SIZE
};

/*
 *	Data declaration
 */

extern struct _option_	option[];

/*
 *	Function declaration
 */

#ifdef __cplusplus
extern "C" {
#endif

void puthelp(void);

#ifdef __cplusplus
}
#endif

#endif /* _OPT_H */
