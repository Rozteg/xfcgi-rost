#ifndef _TASK_H
#define _TASK_H



/*
 *	Macros difinition
 */



/*
 *	Type declaration
 */

struct _packb_ {
	char		*byte;
	int		length;
};

/*
 *	Data declaration
 */

extern char 		packofjson[2000];

/*
 *	Function declaration
 */
 
#ifdef __cplusplus
extern "C" {
#endif

	void *task1(void *arg);
	void ToLocalhost (char *requestpointer, int len, int prt);

#ifdef __cplusplus
}
#endif

#endif /* _NAME_H */