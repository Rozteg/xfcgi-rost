#ifndef _VERSION_H
#define _VERSION_H

/*
 *	Macros difinition
 */

#define _BASE_         "0"
#define _RELEASE_      "1"
#define _BUILD_        "47"

#define _PROJECT_      "xF.fcgi"
#define _DESCRIPTION_  "FCGI - example, testing, template"
#define _SUBJECT_      ""
#define _AUTHOR_       "Andrew Dronov"
#define _VERSION_      ( _BASE_"."_RELEASE_"."_BUILD_" "_BUILD_TIME_  )
#define _BUILD_TIME_   "(Fri Nov  7 09:03:43 MSK 2014)"

#endif /* _VERSION_H */
