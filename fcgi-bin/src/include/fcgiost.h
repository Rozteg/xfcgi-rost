#ifndef _FCGI_OR_STDIO_H
#define _FCGI_OR_STDIO_H

#if defined ( _FCGI_IO_ )

#include <fcgi/fcgi_stdio.h>
#include <fcgi/fcgiapp.h>

#else

#include <stdio.h>
#include <unistd.h>

#endif



/*
 *	Macros difinition
 */



/*
 *	Type declaration
 */



/*
 *	Data declaration
 */



/*
 *	Function declaration
 */

#ifdef __cplusplus
extern "C" {
#endif


#ifdef __cplusplus
}
#endif

#endif /* _FCGI_OR_STDIO_H */